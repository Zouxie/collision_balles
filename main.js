const viewportWidth = window.innerWidth;
const viewportHeight = window.innerHeight;

let originalBall = document.createElement("div");
let ballNumber = 0;
originalBall.setAttribute("class", "ball");
originalBall.setAttribute("id", "ball" + ballNumber);
document.body.append(originalBall);

let numberOfBalls = parseInt(window.prompt("How many balls on screen", 9), 10);

//Array to define all possible collision between balls to avoid a bug where they got stuck in each other. Maybe review the clearCouple function, not sure it works as intended.
let coupleBallsCollision = new Array(numberOfBalls);
for (let i = 0; i < coupleBallsCollision.length; i++) {
  coupleBallsCollision[i] = Array(coupleBallsCollision.length - i - 1)
    .fill(null)
    .map(() => "");
}

//Instantiation of ball objects
let arrayOfBalls = new Array(numberOfBalls)
  .fill(null)
  .map(() => Object.create(ball));

//Represent the arrayOfBalls array in HTML
let arrayOfBalls_html = new Array(numberOfBalls).fill(null);

let arrayWithOnlyCurrentBall = null;

//getStats needed, do not work properly at instantiation ?
for (let i = 0; i < numberOfBalls; i++) {
  arrayOfBalls[i].getStats();
}

//add nodes ball in HTML
duplicate(numberOfBalls);
for (let i = 0; i < numberOfBalls; i++) {
  // Récupération des éléments dans le HTML
  arrayOfBalls_html[i] = document.querySelector("#ball" + i);
  // Définition de leurs point de départ
  arrayOfBalls_html[i].style.left = arrayOfBalls[i].position_x;
  arrayOfBalls_html[i].style.top = arrayOfBalls[i].position_y;
  arrayOfBalls[i].id = arrayOfBalls_html[i].id;
}

let buttonRestart = document.querySelector(".buttonRestart");
buttonRestart.addEventListener("click", function () {
  location.reload();
});

//The more bullet there are, the faster it goes ?? Low level solution with swith
let interval;
// let ratioSpeedBall = numberOfBalls/10;
// interval = ratioSpeedBall * 40; implement a math function to keep speed of balls ?

switch (true) {
  case numberOfBalls < 5:
    interval = 28;
    break;

  case numberOfBalls < 10:
    interval = 45;
    break;

  case numberOfBalls < 15:
    interval = 60;
    break;

  case numberOfBalls < 20:
    interval = 90;
    break;

  case numberOfBalls < 26:
    interval = 60;
    break;

  case numberOfBalls === 60:
    interval = 100;
    break;
}

const pong_preshow = setInterval(intervalFunction, interval);
function intervalFunction() {
  // Button to stop balls movement
  let bouton = document.querySelector(".button");
  bouton.addEventListener("click", function () {
    clearInterval(pong_preshow);
  });

  //for each ball
  for (let i = 0; i < numberOfBalls; i++) {
    let arrayOfBallId = parseInt(arrayOfBalls[i].id.replace(/\D/g, ""));
    //Bounce on the edge of the screen
    if (arrayOfBalls[i].position[0][0] <= 0) {
      arrayOfBalls[i].direction_x = true;
      clearCoupleWithId(arrayOfBallId);
    } else if (arrayOfBalls[i].position[0][1] >= viewportWidth) {
      arrayOfBalls[i].direction_x = false;
      clearCoupleWithId(arrayOfBallId);
    }

    if (arrayOfBalls[i].position[1][0] <= 0) {
      arrayOfBalls[i].direction_y = true;
      clearCoupleWithId(arrayOfBallId);
    } else if (arrayOfBalls[i].position[1][1] >= viewportHeight) {
      arrayOfBalls[i].direction_y = false;
      clearCoupleWithId(arrayOfBallId);
    }

    arrayWithOnlyCurrentBall = arrayOfBalls.slice(i, i + 1); //Slice() tips: If end is greater than the length of the sequence, slice () will extract until the end of the sequence.

    for (let j = 0; j < arrayOfBalls.length; j++) {
      let arrayOfBallsId = parseInt(arrayOfBalls[j].id.replace(/\D/g, ""));
      let arrayWithOnlyCurrentBallId = parseInt(
        arrayWithOnlyCurrentBall[0].id.replace(/\D/g, "")
      );

      if (arrayOfBallsId > arrayWithOnlyCurrentBallId) {
        //I order them to push and remove them in the coupleBallsCollision array
        let tmp = arrayOfBallsId;
        arrayOfBallsId = arrayWithOnlyCurrentBallId;
        arrayWithOnlyCurrentBallId = tmp;
      }

      //If collision
      if (
        j != i &&
        //Horizontally
        ((arrayWithOnlyCurrentBall[0].position[0][0] >
          arrayOfBalls[j].position[0][0] && // if xMin(current ball) is inside another ball(between xMin and xMax)
          arrayWithOnlyCurrentBall[0].position[0][0] <
            arrayOfBalls[j].position[0][1]) ||
          (arrayWithOnlyCurrentBall[0].position[0][1] >
            arrayOfBalls[j].position[0][0] && // OR if xMax is inside another ball(xMin and xMax)
            arrayWithOnlyCurrentBall[0].position[0][1] <
              arrayOfBalls[j].position[0][1])) && // AND verticcaly
        ((arrayWithOnlyCurrentBall[0].position[1][0] >
          arrayOfBalls[j].position[1][0] && // if yMin is inside another ball(yMin and yMax)
          arrayWithOnlyCurrentBall[0].position[1][0] <
            arrayOfBalls[j].position[1][1]) ||
          (arrayWithOnlyCurrentBall[0].position[1][1] >
            arrayOfBalls[j].position[1][0] && // OR if yMax is inside another ball(yMin and yMax)
            arrayWithOnlyCurrentBall[0].position[1][1] <
              arrayOfBalls[j].position[1][1]))
      ) {
        //Are balls already collisionned ?
        if (
          coupleBallsCollision[arrayOfBallsId][
            arrayWithOnlyCurrentBallId - (arrayOfBallsId + 1)
          ] !==
          arrayOfBallsId.toString() + arrayWithOnlyCurrentBallId.toString()
        ) {
          //add more accuracy in bounce (90° change)
          if (
            arrayWithOnlyCurrentBall[0].direction_x ===
              arrayOfBalls[j].direction_x &&
            arrayWithOnlyCurrentBall[0].direction_y !==
              arrayOfBalls[j].direction_y
          ) {
            arrayWithOnlyCurrentBall[0].direction_y =
              !arrayWithOnlyCurrentBall[0].direction_y; // if collision, change of direction
            arrayOfBalls[j].direction_y = !arrayOfBalls[j].direction_y;
          } else if (
            arrayWithOnlyCurrentBall[0].direction_y ===
              arrayOfBalls[j].direction_y &&
            arrayWithOnlyCurrentBall[0].direction_x !==
              arrayOfBalls[j].direction_x
          ) {
            arrayWithOnlyCurrentBall[0].direction_x =
              !arrayWithOnlyCurrentBall[0].direction_x; // if collision, change of direction
            arrayOfBalls[j].direction_x = !arrayOfBalls[j].direction_x;
          } else {
            arrayWithOnlyCurrentBall[0].direction_x =
              !arrayWithOnlyCurrentBall[0].direction_x; // if collision, change of direction
            arrayOfBalls[j].direction_x = !arrayOfBalls[j].direction_x;
            arrayWithOnlyCurrentBall[0].direction_y =
              !arrayWithOnlyCurrentBall[0].direction_y; // if collision, change of direction
            arrayOfBalls[j].direction_y = arrayOfBalls[j].direction_y;
          }
          clearCoupleWithId(arrayOfBallsId);
          clearCoupleWithId(arrayWithOnlyCurrentBallId);
          addCoupleWithId(arrayOfBallsId, arrayWithOnlyCurrentBallId);
        }
      }
      move(
        j,
        (arrayOfBalls[j].direction_x ? 1 : -1) * arrayOfBalls[j].speed_x,
        (arrayOfBalls[j].direction_y ? 1 : -1) * arrayOfBalls[j].speed_y
      );
      refreshPosition(
        j,
        arrayOfBalls[j].position_x,
        arrayOfBalls[j].position_y
      );
    }
  }
}
