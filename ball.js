let ball = Object.assign(Object.create(defaultBall),
    {
        id: null,
        speed_x: 1,
        speed_y: 1,
        collision: true,
        name: undefined,

        direction_x: true, // Correspond à un mouvement positif +1px
        direction_y: true,

        position_x: 110,
        position_y: 110,
        position: [[100, 140], [100, 140]] //xMin, xMax, yMin, Ymax sont les valeurs virtuelles pour gérer les collisions.



    })