function duplicate(nbClone) {

    for (let i = 1; i < nbClone; i++) {
        let clone = originalBall.cloneNode(true);
        clone.id = "ball" + ++ballNumber;
        originalBall.parentNode.appendChild(clone);
    }
}

function move(j, x, y) {
    arrayOfBalls[j].position_x += x;
    arrayOfBalls[j].position_y += y;
    arrayOfBalls_html[j].style.left = arrayOfBalls[j].position_x + "px";
    arrayOfBalls_html[j].style.top = arrayOfBalls[j].position_y + "px";
}

function refreshPosition(j, x, y) {
    arrayOfBalls[j].position = calculPosition(x, y);
}

function addCoupleWithId(id1, id2) {
    coupleBallsCollision[id1][id2 - (id1 + 1)] = id1.toString() + id2.toString();
}

function clearCoupleWithId(id) {
    for (let i = 0; i < coupleBallsCollision[id].length; i++) {
        coupleBallsCollision[id][i] = "";
    }
}

function reverseString(str) {
    return (str === '') ? '' : reverseString(str.substr(1)) + str.charAt(0);
}

function randomNumber(min, max) {
    let range = max - min + 1;
    return Math.floor(Math.random() * range) + min;
}

function getRandomSpeed_x() {
    return randomNumber(1, 2);
}

function getRandomSpeed_y() {
    return randomNumber(1, 2);
}

function getRandomPos_x() {
    return randomNumber(0, window.innerWidth);
}

function getRandomPos_y() {
    return randomNumber(0, window.innerHeight);
}

function getRandomDirection_x() {
    return (Math.random() >= 0.5);
}

function getRandomDirection_y() {
    return (Math.random() >= 0.5);
}

function calculPosition(x, y) { //Une balle fait 40px de large et 30px de haut
    let dimensionX = [x, x + 40];
    let dimensionY = [y, y + 30];

    return [dimensionX, dimensionY]; // Renvoi un tableau des coordonés visuelles de colision
}






