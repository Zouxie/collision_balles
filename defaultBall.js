let defaultBall = {

    getStats: function () {

        this.speed_x = getRandomSpeed_x();
        this.speed_y = getRandomSpeed_y();
        this.position_x = getRandomPos_x();
        this.position_y = getRandomPos_y();
        this.position = calculPosition(this.position_x, this.position_y);
        this.direction_x = getRandomDirection_x();
        this.direction_y = getRandomDirection_y();
    }
}